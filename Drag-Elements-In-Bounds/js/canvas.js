(function() {
  "use strict";

  var animation = (function(){
    "use strict";

    var game = document.getElementById('animation-canvas'),
        gameCtx = game.getContext('2d'),
        cWidth = 300,
        cHeight = 200,
        x = 0,
        y = 0;

    var update = function() {
      // Update cords
      x += 0.2;
      y += 0.2;
    };

    var render = function() {
      // Render a background color
      gameCtx.fillStyle = "#000099";
      gameCtx.fillRect(0, 0, cWidth, cHeight);

      // Fill in the square
      gameCtx.fillStyle="#ff0000";
      gameCtx.fillRect(x, y, 20, 20);
    };

    return {
      render: render,
      update: update
    };

  })();

  window.onload = function() {
    /*Aniamtion update, and render loop*/
    var update = function() {
      animation.update();
    };

    var render = function() {
      animation.render();
    };

    var gameLoop = function() {
      update();
      render();
      window.requestAnimationFrame(gameLoop);
    };
    gameLoop();
  };

}());
