(function() {
  "use strict";

  // Keep elm inside of window/container
  var elmMoveOnResize = function() {
    var dragableCount = document.getElementsByClassName('dragable').length,
        bounds = getBounds();

    for(var i=0; i < dragableCount; i++) {

      var currDiv = document.getElementsByClassName('dragable')[i];

      if(currDiv.offsetTop <= bounds.top) {
        currDiv.style.top = bounds.top + 'px';
      }
      if (currDiv.offsetTop+currDiv.offsetHeight > bounds.bottom) {
        currDiv.style.top = bounds.bottom-currDiv.offsetHeight + 'px';
      }
      if(currDiv.offsetLeft <= bounds.left) {
        currDiv.style.left = bounds.left + 'px';
      }
      if (currDiv.offsetLeft+currDiv.offsetWidth > bounds.right) {
        currDiv.style.left = bounds.right-currDiv.offsetWidth + 'px';
      }
    }
  };

  // Return container bounds values
  var getBounds = function(container) {
    container = container || document.getElementById('container');

    var bounds = container.getBoundingClientRect();

    return {
      left: bounds.left,
      right: bounds.right,
      top: bounds.top,
      bottom: bounds.bottom
    };
  };

  // Allow for grabbing dragable elements
  var elmGrab = function(e){
    var moveReference;

    if(e.target.classList.contains('dragable')) {
      var target = e.target,
          offsetX = e.clientX - target.offsetLeft,
          offsetY = e.clientY - target.offsetTop;

      window.addEventListener('mousemove', moveReference = function(e) {
        elmMove(e, target, offsetX, offsetY);
      }, false);

      window.addEventListener('mouseup', function() {
        window.removeEventListener('mousemove', moveReference, false);
      }, false);
    } else {
      return;
    }
  };

  // Move Element that is grabbed
  var elmMove = function(e, elm, left, top) {
    var bounds = getBounds();
    var target = {};

    target.left   = e.clientX - left;
    target.top    = e.clientY - top;
    target.right  = target.left + elm.offsetWidth;
    target.bottom = target.top + elm.offsetHeight;

    if(target.left < bounds.left) {
      target.left = bounds.left;
    }
    if(target.top < bounds.top) {
      target.top = bounds.top;
    }
    if(target.right > bounds.right) {
      target.left = bounds.right-elm.offsetWidth;
    }
    if(target.bottom > bounds.bottom) {
      target.top = bounds.bottom-elm.offsetHeight;
    }

    elm.style.top = target.top + 'px';
    elm.style.left = target.left + 'px';
  };

  // Setting up listeners for use in the above code, gets called in main.js
  var dragOnLoad = function() {
    var container = document.getElementById('container');

    container.addEventListener('mousedown', elmGrab, false);

    // Keeps element inside of resizing container
    window.addEventListener('resize', elmMoveOnResize, false);
  };

  dragOnLoad();

})();
