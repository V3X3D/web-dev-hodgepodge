# Web Dev Hodgepodge -- Useful snippets of code for web development

## What is it?

A collection of standalone web development projects that are intended combination into larger projects. Why reprogram something from scratch when you can reference an example?

## Overview

`List Tree Navigation`
An expandable list that simulates a file directory structure in a tree view.

`Drag Elements In Bounds`
Create "windows" that you can drag around, set a container that they cannot leave -- like a desktop GUI.

## Support

Like these snippets?

Help show your support for as little as a dollar each month [on Patreon](https://www.patreon.com/V3X3D).
