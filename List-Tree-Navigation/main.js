// Note: <span> elements used to expand the following <ul> element.
window.onload = function() {
  var listElements = document.querySelectorAll(".nav-tree li");

  for(var i = 0; i < listElements.length; i++) {
    if(listElements[i].firstChild.nodeName === 'SPAN') {
      // If 'li' does contain a span tag, it is a folder.
      listElements[i].firstChild.textContent += "/";
      listElements[i].firstChild.onclick = showUL;
      listElements[i].insertAdjacentHTML('afterbegin', '> ');
    } else {
      // If 'li' does NOT contain a span tag, it is a file.
      listElements[i].insertAdjacentHTML('afterbegin', '* ');
    }

    listElements[i].addEventListener("mouseover", function(e) {
      e.stopPropagation();
      this.style.color = "green";
    }, false);

    listElements[i].addEventListener("mouseout", function(e) {
      e.stopPropagation();
      this.style.color = "black";
    }, false);
  }

  function showUL() {
    // Get <ul> following a <span> tag
    var nextUL = this.nextElementSibling;

    // Toggle between displaying and not displaying selected <ul> tag 'nextUL'
    if(nextUL.style.display === "block") {
      nextUL.style.display = "none";
    } else {
      nextUL.style.display = "block";
    }
  }
};
